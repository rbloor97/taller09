/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taller9;

import Comportamiento.ObserverTipoAutorizacionSRI;
import Comportamiento.Offline;
import Comportamiento.Onnline;
import Creacional.FacturaCreator;
import Creacional.GuiaCreator;
import Creacional.NotaCreator;
import Estructural.Factura;
import Estructural.GuiaRemision;
import Estructural.NotaCredito;

/**
 *
 * @author User-PC
 */
public class Taller9 {

    /**
     * @param args the command line arguments
     */
    public static int op=0;
    
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println("Ingrese comprobante desea realizar ? ");
        System.out.println("1. Factura ");
        System.out.println("2. Guia de Remision ");
        System.out.println("3. Nota de credito");
        
        ObserverTipoAutorizacionSRI tipo1=new Offline();
        ObserverTipoAutorizacionSRI tipo2=new Onnline();
        
        switch (op){
            case 1:
               Factura factura=new Factura();
               System.out.println("Ingresando Factura...");
               FacturaCreator facturaCreator=new FacturaCreator();
               facturaCreator.factoryMethod(factura);
               System.out.println("Validando datos factura...");
               tipo1.GenerarAutorizacion();
               System.out.println("Factura autorizada usando offline");
               break;
            case 2:
               GuiaRemision guia=new GuiaRemision();
               System.out.println("Ingresando Guia Remision ...");
               GuiaCreator guiaCreator=new GuiaCreator();
               guiaCreator.factoryMethod(guia);
               System.out.println("Validando datos Guia Remision...");
               tipo2.GenerarAutorizacion();
               System.out.println("Guia autorizada usando Onnline");
               break;
            case 3:
               NotaCredito notaCredito=new NotaCredito();
               System.out.println("Ingresando Nota de Credito ...");
               NotaCreator notaCreator=new NotaCreator();
               notaCreator.factoryMethod(notaCredito);
               System.out.println("Validando datos de Nota de Credito...");
               ObserverTipoAutorizacionSRI tipo=new Offline();
               tipo1.GenerarAutorizacion();
               System.out.println("Nota Credito autorizada usando offline");
               break;
            default:
               System.out.println("No se ingreso comprobante...");
                break;
                  
        }
       
        
        
    }
    
}
