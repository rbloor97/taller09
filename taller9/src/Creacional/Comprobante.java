/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Creacional;

import java.util.Date;

/**
 *
 * @author User-PC
 */
public interface Comprobante {
    
        public void solitarFecha();
        public void solitarDetalleEmisor();
        public void solicitarClaveAcceso();
        public void solicitarNumeroAtorizacion();
    
}
